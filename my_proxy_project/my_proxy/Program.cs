﻿using System;
using my_proxy.RequestHandlers;
using my_proxy.Utils;

namespace my_proxy
{
    class Program
    {
        /// <summary>
        /// Proxy server
        /// </summary>
        static void Main(string[] args)
        {
            FileReader fileReader = new FileReader();

            // --------- Configuration Variables ---------
            Configuration conf = Configuration.Read(fileReader);

            if (conf == null)
            {
                return;
            }

            Console.WriteLine("###############################");
            Console.WriteLine("Proxy Server " + conf.ProxyVersion + " has started.");
            Console.WriteLine("###############################");

            //// ------------- Output black list --------------
            //Console.WriteLine("\nBlack list:");
            //fileReader.ReadListEncrypt(conf.BlackFile).ForEach(Console.WriteLine);

            //static black and white list
            OpenRequestHandler.blackList = fileReader.ReadListEncrypt(conf.BlackFile);
            OpenRequestHandler.whiteList = fileReader.ReadListEncrypt(conf.WhiteFile);

            // ------- Accept/Handle incoming requests ------- 
            new ProxyListener(conf, fileReader).HandleReq();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using my_proxy.Utils;

//holds all the data the different classes need(file names,passwords...)

namespace my_proxy.RequestHandlers
{
    /// <summary>
    /// Configuratoin parameters - Do not change here!
    /// changes are only in the main program.
    /// </summary>
    public class Configuration
    {
        private const string CONFIG_FILE_NAME = "Configuration.ini";

        /// <summary>
        /// proxy host
        /// </summary>
        public String Host { get; set; }

        /// <summary>
        /// proxy port
        /// </summary>
        public String Port { get; set; }

        /// <summary>
        /// Proxy server version
        /// </summary>
        public string ProxyVersion { get; set; }

        /// <summary>
        /// choose state - open / Anonymous (open is the deault) 
        /// </summary>
        //public ProxyMode Mode { get; set; }
        public string Mode { get; set;}

        /// <summary>
        /// White list
        /// </summary>
        public string WhiteFile { get; set; }

        /// <summary>
        /// Black list
        /// </summary>
        public string BlackFile { get; set; }
        
        /// <summary>
        /// White password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Number of packets
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Number of seconds
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Read the configuration file
        /// </summary>
        /// <param name="fileReader">The file reader object</param>
        /// <returns>The configuration file filled with all the parameters</returns>
        /// <remarks>NULL returened if error occured</remarks>
        public static Configuration Read(FileReader fileReader)
        {
            Configuration result = new Configuration();

            try
            {
                List<string> parameters = fileReader.ReadFullUnEncryptedFile(CONFIG_FILE_NAME);
                foreach (string parameter in parameters)
                {
                    #region Add parameter

                    string[] nameValue = parameter.Split('=');

                    switch (nameValue[0].ToUpper())
                    {
                        case "HOST":
                            result.Host = nameValue[1];
                            break;
                        case "PORT":
                            result.Port = nameValue[1];
                            break;
                        case "PROXY_VERSION":
                            result.ProxyVersion = nameValue[1];
                            break;
                        case "MODE":
                            result.Mode =  nameValue[1];
                            break;
                        case "PASSWORD":
                            result.Password = nameValue[1];
                            break;
                        case "BLACK_FILE":
                            result.BlackFile = nameValue[1];
                            break;
                        case "WHITE_FILE":
                            result.WhiteFile = nameValue[1];
                            break;
                        case "X":
                            result.X = Int32.Parse(nameValue[1]);
                            break;
                        case "Y":
                            result.Y = Int32.Parse(nameValue[1]);
                            break;
                    }

                    #endregion
                }
            }
            catch
            {
                Console.WriteLine("###############################");
                Console.WriteLine("Error in conf read");
                Console.WriteLine("###############################");
                result = null;
            }


            return result;
        }
    }
}

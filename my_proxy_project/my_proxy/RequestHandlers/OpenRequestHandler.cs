﻿using System;
using System.IO;
using System.Net;
using System.Text;
using my_proxy.Utils;
using System.Threading;
using System.Collections.Generic;
using System.Net.Security;
using System.Text.RegularExpressions;

namespace my_proxy.RequestHandlers
{
    /// <summary>
    ///  Handle one request of the client from the server.
    /// </summary>
    public class OpenRequestHandler
    {
        protected HttpListenerContext Context;
        protected FileReader FileReader;
        //protected object FileLock;
        protected Configuration Conf;

        protected bool IsAnonymous;
        protected Mutex FileLock;
        private Dictionary<string, List<DateTime>> requestsDic; //keeps the requests and datetimes to check the y,x
        public static List<string> whiteList;
        public static List<string> blackList;
        private const string USER_AGENT = "Mozilla/1.0 (compatible; MSIE 1.00; Windows 3.11)";




        /// <summary>
        /// Initialize the communication with the Remote Server - Open mode
        /// </summary>
        /// <param name="cf">The configuration file</param>
        /// <param name="context">The context of the request</param>
        /// <param name="fr">The file reader object</param>
        /// <param name="fileLock">A lock for synchonization</param>
        public OpenRequestHandler(Configuration cf,
            HttpListenerContext context, FileReader fr, Mutex fileLock, bool IsAnonymous)
        {
            this.Context = context;
            this.RemoteUrl = this.Context.Request.RawUrl;
            this.FileReader = fr;
            this.FileLock = fileLock;
            this.Conf = cf;
            this.IsAnonymous = IsAnonymous;
            //this.blackList = FileReader.ReadListEncrypt(cf.BlackFile);
            //this.whiteList = FileReader.ReadListEncrypt(cf.WhiteFile);
            this.requestsDic = new Dictionary<string, List<DateTime>>();
            if (context.Request.RemoteEndPoint != null)
            {
                fileLock.WaitOne();//lock the log file
                Log.Info(context.Request.RemoteEndPoint.Address + " " + context.Request.RawUrl);
                fileLock.ReleaseMutex();//release the log file
            }
            else
            {
                string contextErrMsg = "Context error in: " + context.Request.RawUrl;
                fileLock.WaitOne();//lock the log file
                Log.Info(contextErrMsg);
                fileLock.ReleaseMutex();//release the log file
                throw new Exception(contextErrMsg);
            }
        }

        /// <summary>
        /// Main method - Method calls when client request the server
        /// </summary>
        public void ProcessRequest()
        {
            // Create a request with same data in navigator request
            HttpWebRequest request = this.GetRequest();
            if (request != null)
            {
                try
                {
                    string ip = Context.Request.UserHostAddress;

                    // check if there are more then 1 requset in distance Y
                    checkYsecondsViolation(ip);

                    //save this request in request dictionnary 
                    requestsDic[ip].Add(DateTime.Now);

                    //checked if in distance Y there are more than x packages
                    checkXpackagesViolation(ip);

                    //create response msg from the request and send it
                    generateResponseMSG(request);

                }

                catch (Exception)
                {
                    Console.WriteLine("Did not complete handling request");
                }
            }
        }

        /// <summary>
        /// create response msg from the request and send it
        /// </summary>
        /// <param name="request"></param>
        private void generateResponseMSG(HttpWebRequest request)
        {
            // Send the request to the remote server and return the response
            HttpWebResponse response = this.GetResponse(request);
            if (response != null)
            {   // if the server return a response - manipulate it (add cookies, etc.) 
                // and send it back to the client
                byte[] responseData = this.GetResponseStreamBytes(response);
                //if it's valid (doesn't have email address)
                if (responseData != null)
                {
                    // transfer cookies to the client from the server...

                    // send the response back to the client
                    this.SendResponse(response, responseData);
                }
            }
        }

        /// <summary>
        /// check if there are more then 1 requset in distance Y
        /// </summary>
        /// <param name="ip"></param>
        private void checkXpackagesViolation(string ip)
        {
            if (requestsDic[ip].Count > Conf.X)
            {
                //if true - remove from white list
                whiteList.Remove(ip);
                //this.FileLock.WaitOne();
                FileReader.UpdateWhiteList(whiteList, Conf.WhiteFile);
                //this.FileLock.ReleaseMutex();
                requestsDic.Remove(ip);
            }
        }
        /// <summary>
        /// checked if in distance Y there are more than x packages
        /// </summary>
        /// <param name="ip"></param>
        private void checkYsecondsViolation(string ip)
        {
            if (requestsDic.ContainsKey(ip))
            {
                foreach (DateTime DT in requestsDic[ip])
                {
                    if (DT.Subtract(DateTime.Now).Seconds > Conf.Y)
                    {
                        requestsDic[ip].Remove(DT);
                    }
                }
            }
            else { requestsDic.Add(ip, new List<DateTime>()); }
        }


        /// <summary>
        /// Return address to communicate to the remote server
        /// </summary>
        public string RemoteUrl { get; private set; }

        /// <summary>
        /// Create a request the remote server
        /// </summary>
        /// <returns>Request to send to the server </returns>
        protected virtual HttpWebRequest GetRequest()
        {

            // Create a request to the server
            HttpWebRequest requestToServer = (HttpWebRequest)WebRequest.Create(this.RemoteUrl);

            // Set some options
            requestToServer.Method = this.Context.Request.HttpMethod;
            requestToServer.KeepAlive = requestToServer.KeepAlive;
            requestToServer.Proxy = null;

            //update User-Agent to anonymous
            if (IsAnonymous)
            {
                requestToServer.UserAgent = USER_AGENT;
            }
            else//Open mode
            {
                requestToServer.UserAgent = this.Context.Request.UserAgent;
                addHeaders(requestToServer);
            }

            // For POST, write the post data extracted from the incoming request
            if (requestToServer.Method == "POST")
            {
                const int bufferSize = 1024;
                Stream clientStream = this.Context.Request.InputStream;
                byte[] clientPostData = new byte[bufferSize];
                requestToServer.ContentType = this.Context.Request.ContentType;
                int read = clientStream.Read(clientPostData, 0, bufferSize);
                StringBuilder data = new StringBuilder();
                Stream stream = requestToServer.GetRequestStream();
                //read data until the end of the stream and write it to the rquest sended
                while (read > 0)
                {
                    string tempString = Encoding.UTF8.GetString(clientPostData, 0, clientPostData.Length);
                    data.Append(tempString);
                    stream.Write(clientPostData, 0, read);
                    read = clientStream.Read(clientPostData, 0, bufferSize);
                }
                stream.Close();

                this.FileLock.WaitOne();//lock the file
                Boolean hasEmail = this.hasEmailInside(data.ToString());
                this.FileLock.ReleaseMutex();//unlock the file
                if (hasEmail)
                {
                    generate403Response();
                    return null;
                }

            }
                           
            
            return requestToServer;
        }

        /// <summary>
        /// add cookies, x-forward-for and proxy version
        /// </summary>
        /// <param name="requestToServer"></param>
        private void addHeaders(HttpWebRequest requestToServer)
        {
            requestToServer.CookieContainer = new CookieContainer();
            foreach (Cookie cookie in Context.Request.Cookies)
            {
                if (cookie.Domain == "")
                {
                    cookie.Domain = Context.Request.Url.Host;
                }
                requestToServer.CookieContainer.Add(cookie);
            }

            String ip = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();

            // add x-forward-for
            requestToServer.Headers.Add("x-forwarded-for", ip);
            // add proxy version
            requestToServer.Headers.Add("proxy-version", "1.0");
        }

        /// <summary>
        /// Send the request to the remote server and return the response
        /// </summary>
        /// <param name="request">Request to send to the server </param>
        /// <returns>Response received from the remote server
        ///           or null if page not found </returns>
        private HttpWebResponse GetResponse(HttpWebRequest request)
        {
            HttpWebResponse response;
            String user_IP = Context.Request.UserHostAddress;
            
            try
            {
                //check if the url in black list
                if (blackList.Contains(RemoteUrl))
                {
                    generate403Response();
                    return null;
                }

                // if user not in white list
                else if (!whiteList.Contains(user_IP))
                {
                    Boolean confirm = false;
                    confirm = validateUser(user_IP);
                    if (!confirm){
                        return null;
                    }
                }

                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException)
            {
                // Send 404 to client 
                this.Context.Response.StatusCode = 404;
                this.Context.Response.StatusDescription = "Page Not Found";
                this.Context.Response.Close();
                Console.WriteLine("404: Page Not Found");
                return null;
            }

            return response;
        }

        /// <summary>
        /// check the user is valid 
        /// </summary>
        /// <param name="user_IP"></param>
        /// <returns>true if user entered the right password</returns>
        private Boolean validateUser(string user_IP)
        {
            Console.WriteLine("Please enter your password");
            string password = Console.ReadLine();
            if (password != Conf.Password)
            {
                generate403Response();
                return false;
            }
            else
            {
                writeToWhiteFile(user_IP);
                return true;
            }

        }

        /// <summary>
        /// write user IP to white list encyptedlly 
        /// </summary>
        /// <param name="user_IP"></param>
        private void writeToWhiteFile(string user_IP)
        {
            StreamWriter streanWriter = new StreamWriter(Conf.WhiteFile);
            String decriptedString = TripleDes.EncryptString(user_IP, FileReader.KEY);
            
            streanWriter.WriteLine(decriptedString);
           
            whiteList.Add(user_IP);
        }

        /// <summary>
        /// generate 403 forbidden HTML page to present for user and write it to console 
        /// </summary>
        private void generate403Response()
        {

            string forbiddenStr = "<html>\n<head>\n<title>Error - Forbidden Page </title>\n</head>\n<body>\n<strong><h1> \" 403: Forbidden - you are not authorized!\"</h1></strong>\n</body>\n</html>\n";
            byte[] forbiddenByte = Encoding.UTF8.GetBytes(forbiddenStr);
            Context.Response.OutputStream.Write(forbiddenByte, 0, forbiddenByte.Length);
            this.Context.Response.StatusDescription = "Forbidden";
            this.Context.Response.StatusCode = 403;
            this.Context.Response.Close();
            Console.WriteLine("Forbidden - 403");
        }


        /// <summary>
        /// Return the response in bytes array format
        /// </summary>
        /// <param name="response">Response received
        ///             from the remote server </param>
        /// <returns>Response in bytes </returns>
        private byte[] GetResponseStreamBytes(HttpWebResponse response)
        {
            const int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            MemoryStream memoryStream = new MemoryStream();
            //get the response
            Stream responseStream = response.GetResponseStream();
            //read all the data from the response and save it in a memory stream
            if (responseStream != null)
            {
                int remoteResponseCount = responseStream.Read(buffer, 0, bufferSize);

                while (remoteResponseCount > 0)
                {
                    memoryStream.Write(buffer, 0, remoteResponseCount);
                    remoteResponseCount = responseStream.Read(buffer, 0, bufferSize);
                }
            }
            else
            {
                throw new Exception("Response is null");
            }

            //check for mails in the data and headers of the response
            StringBuilder sb = new StringBuilder();
            byte[] responseData = memoryStream.ToArray();
            string tempString = Encoding.UTF8.GetString(responseData, 0, responseData.Length);
            sb.Append(tempString);


            this.FileLock.WaitOne();//lock the file
            Boolean hasEmail = this.hasEmailInside(responseData.ToString());
            this.FileLock.ReleaseMutex();//unlock the file
            if (hasEmail)
            {
                generate403Response();
                return null;
            }

            //replace BGU ISE SE to relevant words and returns byte[] of the replaced string
            responseData = replaceSelectedWords(tempString);

            // close the open streams
            memoryStream.Close();
            responseStream.Close();
            memoryStream.Dispose();
            responseStream.Dispose();

            return responseData;
        }

        /// <summary>
        /// replace BGU ISE SE to relevant words and returns byte[] of the replaced string
        /// </summary>
        /// <param name="tempString"></param>
        /// <returns></returns>
        private byte[] replaceSelectedWords(string tempString)
        {
            Dictionary<string, string> words = new Dictionary<string,string>();
            words.Add("BGU", "BenGurion");
            words.Add("ISE", "InformationSystemEngineering");
            words.Add("SE", "SoftwareEngineering");

            foreach (string word in words.Keys)
            {
                MatchCollection matchesCollection  = Regex.Matches(tempString, word, RegexOptions.IgnoreCase);

                if (matchesCollection.Count > 0)
                {
                    foreach (Match match in matchesCollection)
                    {
                        FileLock.WaitOne();//lock the log file
                        Log.Info("Word transformed: " +match.ToString() + " -> " + words[word]);
                        FileLock.ReleaseMutex();//release the log file
                    }
                }
                tempString = Regex.Replace(tempString, word, words[word], RegexOptions.IgnoreCase);
            }
            
            return Encoding.ASCII.GetBytes(tempString);
        }

        /// <summary>
        /// check if request has email addresses inside
        /// </summary>
        public Boolean hasEmailInside(string userInput)
        {
           Boolean hasEmail = false;

            try
            {
                string regularExpression = @"\b[A-Z0-9._-]+@[A-Z0-9][A-Z0-9.-]{0,61}[A-Z0-9]\.[A-Z.]{2,6}\b";
                string wholeRequestStr = Context.Request.RawUrl + " " + userInput + " " + Context.Request.Headers.ToString(); 
                wholeRequestStr = wholeRequestStr.Replace("%40", "@");
                MatchCollection matchesCollection = Regex.Matches(wholeRequestStr, regularExpression, RegexOptions.IgnoreCase);

                //write to file
                if (matchesCollection.Count > 0)
                {
                    foreach (Match match in matchesCollection)
                    {
                        FileLock.WaitOne();//lock the log file
                        Log.Info("Email blocked: "+ match.ToString());
                        FileLock.ReleaseMutex();//release the log file
                        hasEmail = true;
                    }
                }
            }

            catch (Exception e)
            {
                throw e;
            }
           
            return hasEmail;
        }


        /// <summary> 
        ///  send the response back to the client
        /// </summary>
        /// <param name="response"> the response to send back </param>
        /// <param name="responseData"> the response data to send back </param>
        private void SendResponse(HttpWebResponse response, byte[] responseData)
        {
            // Send the response to client
            this.Context.Response.ContentEncoding = Encoding.UTF8;
            this.Context.Response.ContentType = response.ContentType;
            try
            {
                this.Context.Response.OutputStream.Write(responseData, 0, responseData.Length);
                this.Context.Response.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("could not finish proxy request");
            }
        }

    }
}

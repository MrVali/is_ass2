﻿using System.Net;
using System.Threading;
using my_proxy.Utils;

namespace my_proxy.RequestHandlers
{
    /// <summary>
    /// Handler all Client's requests and deliver the web site
    /// </summary>
    public class ProxyListener
    {
        //the conf that holds all the configartion data
        Configuration _conf;
        //File Reader that reads and writes to the whitelist blacklist files 
        FileReader _fileReader;
        //a lock that sync the writing to the files
        //readonly object _fileLock = new object();
        Mutex _fileLock;

        public ProxyListener(Configuration cf, FileReader fr)
        {
            this._conf = cf;
            this._fileReader = fr;
            this._fileLock = new Mutex();
        }
        /// <summary>
        /// the main function,creates a new listener on the given port and sends it contexts to the threads that 
        /// handle the requests
        /// </summary>
        public void HandleReq()
        {
            // Create a listener
            HttpListener listener = new HttpListener();
            // Add the prefix
            listener.Prefixes.Add("http://" + this._conf.Host + ":" + this._conf.Port + "/");
            //add the function that will check if we need to a Authenticate the request
            listener.AuthenticationSchemeSelectorDelegate = this.AuthenticationSchemeForClient;
            //start the listener
            listener.Start();
            while (true)
            {
                // Specify the authentication delegate.
                HttpListenerContext context = listener.GetContext();
                OpenRequestHandler requestHandler;
                //choose the state of the proxy - Open / Anonymous
                switch (this._conf.Mode)
                {
                    default:
                        requestHandler = new OpenRequestHandler(this._conf, context, this._fileReader, this._fileLock,false);
                        break;
                    case "Anonymous_MODE":
                        requestHandler = new OpenRequestHandler(this._conf, context, this._fileReader, this._fileLock,true);
                        break;
                }
                new Thread(requestHandler.ProcessRequest).Start();
            }
        }

        /// <summary>
        /// decides if to ask for Authentication from the user by checking the whitlist
        /// </summary>
        /// <param name="request"></param> the given request
        /// <returns></returns>
        private AuthenticationSchemes AuthenticationSchemeForClient(HttpListenerRequest request)
        {
                return AuthenticationSchemes.Anonymous;
        }

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;

namespace my_proxy.Utils
{
    public class FileReader
    {

        public static string KEY = "abcdefghijklmnop";
        public static byte[] IV = new byte[16];

        /// <summary>
        /// read full file into list
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public List<string> ReadFullUnEncryptedFile(string fileName)
        {
            return System.IO.File.ReadAllLines(fileName).ToList<string>();
        }
      
        /// <summary>
        /// write all the IPs in the whitelist list to filename
        /// </summary>
        /// <param name="whiteList">list of ip of the whitelist </param>
        public void UpdateWhiteList(List<string> whiteList, string filename)
        {
            FileStream fileStream = null;
            StreamWriter streamWiter = null;
            try
            {
                fileStream = new FileStream(filename, FileMode.Create);
                streamWiter = new StreamWriter(fileStream);

                foreach (String ip in whiteList)
                {
                    streamWiter.WriteLine(TripleDes.EncryptString(ip, KEY));
                }
            }
            catch (IOException e) { Console.WriteLine(e); }
            finally
            {
                if (streamWiter != null)
                    streamWiter.Close();
                if (fileStream != null)
                    fileStream.Close();
            }
        }

        /// <summary>
        ///write string eMailText to LogFile
        /// </summary>
        /// <param name="eMailText"></param>
        /// <param name="LogFile"></param>
        public void WriteEmailToLog(string eMailText, string LogFile)
        {
            FileStream fileStream = null;
            StreamWriter streamWiter = null;

            try
            {
                fileStream = new FileStream(LogFile, FileMode.Append);
                streamWiter = new StreamWriter(fileStream);
                streamWiter.WriteLine(eMailText);

            }
            catch (IOException e)
            {
                Console.WriteLine("What are you doing?! You can't write that - Error");
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (streamWiter != null)
                    streamWiter.Close();
                if (fileStream != null)
                    fileStream.Close();
            }

        }


        /// <summary>
        /// read line by line from filename
        /// </summary>
        /// <param name="filename"></param>
        internal List<string> ReadListEncrypt(string filename)
        {
            StreamReader data;
            data = new StreamReader(filename);
            string line = "";
            List<string> returnedList = new List<string>();
            while ((line = data.ReadLine()) != null)
            {
                returnedList.Add(TripleDes.DecryptString(line, KEY));
            }
            if (data != null)
                data.Close();

            return returnedList;
        }



    }
}

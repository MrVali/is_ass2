﻿using System;
using log4net;
using log4net.Config;

namespace my_proxy.Utils
{
    /// <summary>
    /// log class that uses the log4net dll
    /// </summary>
    public class Log : MarshalByRefObject
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(Log));

        static Log()
        {
            XmlConfigurator.Configure();
        }

        public static void Debug(object message)
        {
            _logger.Debug(message);
        }

        public static void Debug(object message, Exception exception)
        {
            _logger.Debug(message, exception);
        }

        public static void Info(object message)
        {
            _logger.Info(message);
        }

        public static void Info(object message, Exception exception)
        {
            _logger.Info(message, exception);
        }

        public static void Warn(object message)
        {
            _logger.Warn(message);
        }

        public static void Warn(object message, Exception exception)
        {
            _logger.Warn(message, exception);
        }

        public static void Error(object message)
        {
            _logger.Error(message);
        }

        public static void Error(object message, Exception exception)
        {
            _logger.Error(message, exception);
        }

        public static void Fatal(object message)
        {
            _logger.Fatal(message);
        }

        public static void Fatal(object message, Exception exception)
        {
            _logger.Fatal(message, exception);
        }
    }
}